" most of these came from http://dougblack.io/words/a-good-vimrc.html
" .. who said ".. Don't put any lines in your vimrc that you don't understand..."

colorscheme peaksea

syntax enable           " enable syntax processing
filetype indent on      " load filetype-specific indent files

set tabstop=4       " number of visual spaces per TAB
set softtabstop=4   " number of spaces in tab when editing
set expandtab       " tabs are spaces

set number              " show line numbers
set showcmd             " show command in bottom bar
set cursorline          " highlight current line

set wildmenu            " visual autocomplete for command menu

set showmatch           " highlight matching [{()}]

set statusline+=%F

